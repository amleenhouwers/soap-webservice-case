package nl.pharmapartners.patient.eipatientservice.error;

public class InvalidBirthDateException extends Exception {

   public InvalidBirthDateException(Throwable cause) {
        super(cause);
    }
}
