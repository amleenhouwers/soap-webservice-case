package nl.pharmapartners.patient.eipatientservice.service.impl;

import nl.pharmapartners.patient.eipatientservice.domain.Patient;
import nl.pharmapartners.patient.eipatientservice.repository.PatientRepository;
import nl.pharmapartners.patient.eipatientservice.service.GetPatientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;

@Service("getPatientService")
public class GetPatientServiceImpl implements GetPatientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetPatientServiceImpl.class);

    @Autowired
    @Qualifier("patientRepository")
    private PatientRepository patientRepository;

    @Override
    @Transactional
    public Patient getPatientByBsn(final BigInteger bsn) {
        LOGGER.info("Retrieving patient from database");

        Patient patient = patientRepository.findByBsn(bsn);
        LOGGER.debug("Retrieved patient from database with data: {}", patient);

        return patient;
    }
}
