package nl.pharmapartners.patient.eipatientservice.service;

import nl.pharmapartners.patient.eipatientservice.domain.Patient;

import java.math.BigInteger;

public interface GetPatientService {

    /**
     * @param bsn unique identifier of patient
     * @return get patient details
     */
    Patient getPatientByBsn(final BigInteger bsn);
}
