package nl.pharmapartners.patient.eipatientservice.business;

import nl.pharmapartners.services.patientservice.v1.GetPatientDetailsResponse;

import java.math.BigInteger;

public interface GetPatientBusiness {

    /**
     * Gets the patientDetailsResponse
     *
     * @param bsn
     * @return get the response containing patient details
     */
    GetPatientDetailsResponse getPatientResponse(BigInteger bsn);
}
