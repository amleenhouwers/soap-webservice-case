package nl.pharmapartners.patient.eipatientservice.error;

public class InvalidGenderException extends Exception {

   public InvalidGenderException(Throwable cause) {
        super(cause);
    }
}
