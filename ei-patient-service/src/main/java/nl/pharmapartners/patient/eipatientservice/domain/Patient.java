package nl.pharmapartners.patient.eipatientservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Calendar;

@Data
@AllArgsConstructor
@Entity
@Table(name = "tbl_patient")
public class Patient {

    @Id
    @Column(name="BSN", columnDefinition = "bigint(9)")
    private BigInteger bsn;

    @Column(name="FIRST_NAME")
    private String firstName;

    @Column(name="NAME")
    private String name;

    @Column(name="INITIALS")
    private String initials;

    @Column(name="BIRTH_DATE")
    @Temporal(TemporalType.DATE)
    private Calendar birthDate;

    @Column(name="GENDER")
    private String gender;

    public Patient(){
        //Default constructor for Hibernate
    }
}
