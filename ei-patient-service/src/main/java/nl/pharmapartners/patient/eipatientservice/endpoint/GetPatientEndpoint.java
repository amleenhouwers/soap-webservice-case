package nl.pharmapartners.patient.eipatientservice.endpoint;

import nl.pharmapartners.patient.eipatientservice.business.GetPatientBusiness;
import nl.pharmapartners.services.patientservice.v1.GetPatientDetailsRequest;
import nl.pharmapartners.services.patientservice.v1.GetPatientDetailsResponse;
import nl.pharmapartners.services.patientservice.v1.ObjectFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import javax.xml.bind.JAXBElement;

@Endpoint
public class GetPatientEndpoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetPatientEndpoint.class);
    private static final String NAMESPACE_URI = "http://www.pharmapartners.nl/services/patientservice/v1";

    @Autowired
    @Qualifier("getPatientBusiness")
    GetPatientBusiness getPatientBusiness;

    /**
     * @param request the SOAP request containing bsn
     * @return returns SOAP response containing patient details
     */
    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetPatientDetailsRequest")
    @ResponsePayload
    public JAXBElement<GetPatientDetailsResponse> getPatient(@RequestPayload JAXBElement<GetPatientDetailsRequest> request) {
        LOGGER.info("Endpoint received call");

        ObjectFactory objectFactory = new ObjectFactory();
        GetPatientDetailsResponse response = getPatientBusiness.getPatientResponse(request.getValue().getBSN());

        return objectFactory.createGetPatientDetailsResponse(response);
    }
}
