package nl.pharmapartners.patient.eipatientservice.business.impl;

import nl.pharmapartners.patient.eipatientservice.business.GetPatientBusiness;
import nl.pharmapartners.patient.eipatientservice.domain.Patient;
import nl.pharmapartners.patient.eipatientservice.error.InvalidBirthDateException;
import nl.pharmapartners.patient.eipatientservice.error.InvalidGenderException;
import nl.pharmapartners.patient.eipatientservice.service.GetPatientService;
import nl.pharmapartners.patient.eipatientservice.util.CalendarConverter;
import nl.pharmapartners.services.patientservice.v1.GenderType;
import nl.pharmapartners.services.patientservice.v1.GetPatientDetailsResponse;
import nl.pharmapartners.services.patientservice.v1.ResultCodeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.math.BigInteger;

@Component("getPatientBusiness")
public class GetPatientBusinessImpl implements GetPatientBusiness {

    private static final Logger LOGGER = LoggerFactory.getLogger(GetPatientBusinessImpl.class);

    @Autowired
    @Qualifier("getPatientService")
    private GetPatientService getPatientServiceImpl;

    @Autowired
    @Qualifier("calendarConverter")
    CalendarConverter converter;

    @Override
    public GetPatientDetailsResponse getPatientResponse(final BigInteger bsn) {
        Patient patient = getPatientServiceImpl.getPatientByBsn(bsn);

        if(patient == null) {
            LOGGER.warn("No patient with bsn {} found", bsn);
            GetPatientDetailsResponse response = new GetPatientDetailsResponse();
            response.setResultCode(ResultCodeType.BAD_REQUEST);

            return response;
        }

        return convertToResponse(patient);
    }

    private GetPatientDetailsResponse convertToResponse(final Patient patient) {
        LOGGER.debug("Converting patient to SOAP response");
        GetPatientDetailsResponse response = new GetPatientDetailsResponse();
        nl.pharmapartners.services.patientservice.v1.Patient responsePatient = new nl.pharmapartners.services.patientservice.v1.Patient();

        responsePatient.setBSN(patient.getBsn());
        responsePatient.setFirstName(patient.getFirstName());
        responsePatient.setName(patient.getName());
        responsePatient.setInitials(patient.getInitials());

        try {
            responsePatient.setBirthdate(converter.toXmlCalendar(patient.getBirthDate()));
            responsePatient.setGender(getGenderType(patient.getGender()));
        } catch (InvalidBirthDateException | InvalidGenderException exception) {
            LOGGER.error("Database contains incorrect data", exception.getCause());
            response.setPatient(null);
            response.setResultCode(ResultCodeType.ERROR);

            return response;
        }
        response.setResultCode(ResultCodeType.OK);
        response.setPatient(responsePatient);

        return response;
    }

    private GenderType getGenderType(final String genderCode) throws InvalidGenderException {
        try {
            return GenderType.fromValue(genderCode);
        } catch (IllegalArgumentException e) {
            throw new InvalidGenderException(e);
        }
    }
}
