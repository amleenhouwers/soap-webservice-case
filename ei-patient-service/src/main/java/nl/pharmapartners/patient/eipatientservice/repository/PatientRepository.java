package nl.pharmapartners.patient.eipatientservice.repository;

import nl.pharmapartners.patient.eipatientservice.domain.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository("patientRepository")
public interface PatientRepository extends JpaRepository<Patient, BigInteger> {

    /**
     * @param bsn unique id of patient
     * @return gets patient found in database
     */
    Patient findByBsn(BigInteger bsn);
}
