package nl.pharmapartners.patient.eipatientservice.util;

import nl.pharmapartners.patient.eipatientservice.error.InvalidBirthDateException;
import org.springframework.stereotype.Component;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Calendar;
import java.util.GregorianCalendar;

@Component("calendarConverter")
public class CalendarConverter {

    /**
     * @param calendar
     * @return gets XMLGregorianCalendar converted from calendar
     * @throws InvalidBirthDateException
     */
    public XMLGregorianCalendar toXmlCalendar(final Calendar calendar) throws InvalidBirthDateException {
        try {
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTime(calendar.getTime());

            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        } catch (DatatypeConfigurationException exception) {
            throw new InvalidBirthDateException(exception.getCause());
        }
    }
}
