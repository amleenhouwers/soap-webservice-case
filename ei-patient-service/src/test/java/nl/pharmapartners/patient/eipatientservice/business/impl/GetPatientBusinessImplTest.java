package nl.pharmapartners.patient.eipatientservice.business.impl;

import nl.pharmapartners.patient.eipatientservice.business.GetPatientBusiness;
import nl.pharmapartners.patient.eipatientservice.domain.Patient;
import nl.pharmapartners.patient.eipatientservice.error.InvalidBirthDateException;
import nl.pharmapartners.patient.eipatientservice.fixture.TestFixtures;
import nl.pharmapartners.patient.eipatientservice.service.GetPatientService;
import nl.pharmapartners.patient.eipatientservice.util.CalendarConverter;
import nl.pharmapartners.services.patientservice.v1.GetPatientDetailsResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static nl.pharmapartners.patient.eipatientservice.fixture.TestFixtures.*;
import static nl.pharmapartners.services.patientservice.v1.ResultCodeType.BAD_REQUEST;
import static nl.pharmapartners.services.patientservice.v1.ResultCodeType.ERROR;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class GetPatientBusinessImplTest {

    @TestConfiguration
    static class GetPatientBusinessTestContextConfiguration {
        @Bean
        public GetPatientBusiness getPatientBusiness() {
            return new GetPatientBusinessImpl();
        }
    }

    @Autowired
    private GetPatientBusiness getPatientBusiness;

    @MockBean
    @Qualifier("getPatientService")
    private GetPatientService getPatientService;

    @MockBean
    @Qualifier("calendarConverter")
    private CalendarConverter converter;

    private Patient patient;

    @Before
    public void setUp() throws InvalidBirthDateException {
        patient = TestFixtures.getPatient();
        Mockito.when(getPatientService.getPatientByBsn(BSN)).thenReturn(patient);
        Mockito.when(converter.toXmlCalendar(getBirthDate())).thenReturn(getXmlGregorialCalendar());
    }

    @Test
    public void getPatientResponse_whenPatientIsFound_createAndReturnPatientResponse() {
        //given
        GetPatientDetailsResponse expectedResponse = TestFixtures.getGetPatientDetailsResponse();

        //when
        GetPatientDetailsResponse actualResponse = getPatientBusiness.getPatientResponse(BSN);

        //then
        assertThat(expectedResponse).isEqualTo(actualResponse);
    }

    @Test
    public void getPatientResponse_whenPatientIsNotFound_returnBadRequestResponse() {
        //given
        GetPatientDetailsResponse expectedResponse =  new GetPatientDetailsResponse();
        expectedResponse.setResultCode(BAD_REQUEST);

        //when
        GetPatientDetailsResponse actualResponse = getPatientBusiness.getPatientResponse(UNKNOWN_BSN);

        //then
        assertThat(expectedResponse).isEqualTo(actualResponse);
    }

    @Test
    public void getPatientResponse_whenBirthDateCannotBeConverted_returnErrorResponse() throws InvalidBirthDateException {
        //given
        Mockito.when(converter.toXmlCalendar(getBirthDate())).thenThrow(new InvalidBirthDateException(new Throwable()));

        GetPatientDetailsResponse expectedResponse = TestFixtures.getGetPatientDetailsResponse();
        expectedResponse.setPatient(null);
        expectedResponse.setResultCode(ERROR);

        //when
        GetPatientDetailsResponse actualResponse = getPatientBusiness.getPatientResponse(BSN);

        //then
        assertThat(expectedResponse).isEqualTo(actualResponse);
    }

    @Test
    public void getPatientResponse_whenRetrievedGenderCodeIsIncorrect_returnErrorResponse() {
        //given
        GetPatientDetailsResponse expectedResponse =  TestFixtures.getGetPatientDetailsResponse();
        expectedResponse.setPatient(null);
        expectedResponse.setResultCode(ERROR);

        patient.setGender(UNKNOWN_GENDER);

        //when
        GetPatientDetailsResponse actualResponse = getPatientBusiness.getPatientResponse(BSN);

        //then
        assertThat(expectedResponse).isEqualTo(actualResponse);
    }
}
