package nl.pharmapartners.patient.eipatientservice.service.impl;

import nl.pharmapartners.patient.eipatientservice.domain.Patient;
import nl.pharmapartners.patient.eipatientservice.fixture.TestFixtures;
import nl.pharmapartners.patient.eipatientservice.repository.PatientRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static nl.pharmapartners.patient.eipatientservice.fixture.TestFixtures.BSN;
import static nl.pharmapartners.patient.eipatientservice.fixture.TestFixtures.UNKNOWN_BSN;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class GetPatientServiceImplTest {

    @TestConfiguration
    static class GetPatientServiceTestContextConfiguration {
        @Bean
        public GetPatientServiceImpl getPatientService() {
            return new GetPatientServiceImpl();
        }
    }

    @Autowired
    private GetPatientServiceImpl getPatientService;

    @MockBean
    @Qualifier("patientRepository")
    private PatientRepository patientRepository;

    private Patient patient = TestFixtures.getPatient();

    @Before
    public void setUp() {
        Mockito.when(patientRepository.findByBsn(BSN)).thenReturn(patient);
    }

    @Test
    public void getPatientByBsn_whenBsnIsKnown_returnCorrespondingPatient() {
        //given
        Patient expectedPatient = patient;

        //when
        Patient actualPatient = getPatientService.getPatientByBsn(BSN);

        //then
        assertThat(expectedPatient).isEqualTo(actualPatient);
    }

    @Test
    public void getPatientByBsn_whenBsnIsUnknown_returnNull() {
        //given
        Patient expectedPatient = null;

        //when
        Patient actualPatient = getPatientService.getPatientByBsn(UNKNOWN_BSN);

        //then
        assertThat(expectedPatient).isEqualTo(actualPatient);
    }
}
