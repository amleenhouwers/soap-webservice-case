package nl.pharmapartners.patient.eipatientservice.repository;

import nl.pharmapartners.patient.eipatientservice.domain.Patient;
import nl.pharmapartners.patient.eipatientservice.fixture.TestFixtures;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static nl.pharmapartners.patient.eipatientservice.fixture.TestFixtures.BSN;
import static nl.pharmapartners.patient.eipatientservice.fixture.TestFixtures.UNKNOWN_BSN;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class PatientRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    @Qualifier("patientRepository")
    private PatientRepository patientRepository;

    @Test
    public void findByBsn_whenBsnIsKnownInDatabase_returnCorrespondingPatient() {
        //given
        Patient expectedPatient = TestFixtures.getPatient();
        entityManager.persist(expectedPatient);
        entityManager.flush();

        //when
        Patient actualPatient = patientRepository.findByBsn(BSN);

        //then
        assertThat(expectedPatient).isEqualTo(actualPatient);
    }

    @Test
    public void findByBsn_whenBsnIsUnKnownInDatabase_returnNull() {
        //given
        Patient expectedPatient = null;

        //when
        Patient actualPatient = patientRepository.findByBsn(UNKNOWN_BSN);

        //then
        assertThat(expectedPatient).isEqualTo(actualPatient);
    }
}
