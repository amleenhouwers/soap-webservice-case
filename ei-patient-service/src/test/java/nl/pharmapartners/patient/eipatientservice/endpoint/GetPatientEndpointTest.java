package nl.pharmapartners.patient.eipatientservice.endpoint;

import nl.pharmapartners.patient.eipatientservice.business.GetPatientBusiness;
import nl.pharmapartners.patient.eipatientservice.error.InvalidBirthDateException;
import nl.pharmapartners.patient.eipatientservice.fixture.TestFixtures;
import nl.pharmapartners.services.patientservice.v1.GetPatientDetailsRequest;
import nl.pharmapartners.services.patientservice.v1.GetPatientDetailsResponse;
import nl.pharmapartners.services.patientservice.v1.ObjectFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.bind.JAXBElement;

import static nl.pharmapartners.patient.eipatientservice.fixture.TestFixtures.BSN;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class GetPatientEndpointTest {

    @TestConfiguration
    static class GetPatientEndpointTestContextConfiguration {
        @Bean
        public GetPatientEndpoint getPatientEndpoint() {
            return new GetPatientEndpoint();
        }
    }

    @Autowired
    private GetPatientEndpoint getPatientEndpoint;

    @MockBean
    @Qualifier("getPatientBusiness")
    private GetPatientBusiness getPatientBusiness;

    @Before
    public void setUp() throws InvalidBirthDateException {
        GetPatientDetailsResponse response = TestFixtures.getGetPatientDetailsResponse();
        Mockito.when(getPatientBusiness.getPatientResponse(BSN)).thenReturn(response);
    }

    @Test
    public void getPatientResponse_whenPatientIsFound_createAndReturnPatientResponse() {
        //given
        ObjectFactory objectFactory = new ObjectFactory();

        GetPatientDetailsResponse expectedResponse = TestFixtures.getGetPatientDetailsResponse();
        JAXBElement<GetPatientDetailsRequest> request = objectFactory.createGetPatientDetailsRequest(TestFixtures.getPatientDetailsRequest());

        //when
        JAXBElement<GetPatientDetailsResponse>  actualResponse = getPatientEndpoint.getPatient(request);

        //then
        assertThat(expectedResponse).isEqualTo(actualResponse.getValue());
    }
}