package nl.pharmapartners.patient.eipatientservice.util;

import nl.pharmapartners.patient.eipatientservice.error.InvalidBirthDateException;
import nl.pharmapartners.patient.eipatientservice.fixture.TestFixtures;
import org.junit.Before;
import org.junit.Test;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Calendar;

import static org.assertj.core.api.Assertions.assertThat;

public class CalendarConverterTest {

    private CalendarConverter calendarConverter;

    @Before
    public void setup() {
        calendarConverter = new CalendarConverter();
    }

    @Test
    public void getPatientResponse_whenPatientIsFound_createAndReturnPatientResponse() throws InvalidBirthDateException {
        //given
        Calendar calendar = TestFixtures.getBirthDate();
        XMLGregorianCalendar expectedXmlGregorianCalendar = TestFixtures.getXmlGregorialCalendar();

        //when
        XMLGregorianCalendar actualXmlGregorianCalendar = calendarConverter.toXmlCalendar(calendar);

        //then
        assertThat(expectedXmlGregorianCalendar).isEqualTo(actualXmlGregorianCalendar);
    }
}
