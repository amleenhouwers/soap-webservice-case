package nl.pharmapartners.patient.eipatientservice.fixture;


import nl.pharmapartners.patient.eipatientservice.domain.Patient;
import nl.pharmapartners.services.patientservice.v1.GenderType;
import nl.pharmapartners.services.patientservice.v1.GetPatientDetailsRequest;
import nl.pharmapartners.services.patientservice.v1.GetPatientDetailsResponse;
import nl.pharmapartners.services.patientservice.v1.ResultCodeType;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class TestFixtures {
    public static final BigInteger BSN = BigInteger.valueOf(12344321);
    public static final BigInteger UNKNOWN_BSN = BigInteger.valueOf(999999999);
    public static final String FIRST_NAME = "FirstName";
    public static final String NAME = "Name";
    public static final String INITIALS = "Initials";
    public static final String GENDER = "M";
    public static final String UNKNOWN_GENDER = "Z";

    public static Patient getPatient() {
        return new Patient(BSN, FIRST_NAME, NAME, INITIALS, getBirthDate(), GENDER);
    }

    public static Calendar getBirthDate() {
        Calendar birthDate = Calendar.getInstance();
        birthDate.add(Calendar.YEAR, -18);
        birthDate.set(Calendar.HOUR, 0);
        birthDate.set(Calendar.MINUTE, 0);
        birthDate.set(Calendar.SECOND, 0);
        birthDate.set(Calendar.MILLISECOND, 0);

        return birthDate;
    }

    public static GetPatientDetailsRequest getPatientDetailsRequest() {
        GetPatientDetailsRequest request = new GetPatientDetailsRequest();
        request.setBSN(BSN);

        return request;
    }

    public static GetPatientDetailsResponse getGetPatientDetailsResponse() {
        GetPatientDetailsResponse response = new GetPatientDetailsResponse();
        nl.pharmapartners.services.patientservice.v1.Patient patient = new nl.pharmapartners.services.patientservice.v1.Patient();

        patient.setBSN(BSN);
        patient.setFirstName(FIRST_NAME);
        patient.setName(NAME);
        patient.setInitials(INITIALS);
        patient.setBirthdate(getXmlGregorialCalendar());
        patient.setGender(GenderType.M);

        response.setPatient(patient);
        response.setResultCode(ResultCodeType.OK);

        return response;
    }

    public static XMLGregorianCalendar getXmlGregorialCalendar()  {
        try {
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTime(getBirthDate().getTime());

            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        } catch (DatatypeConfigurationException e) {
            return null;
        }
    }
}
