package nl.pharmapartners.patient.eiservice;

import nl.pharmapartners.patient.eipatientservice.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ws.test.server.MockWebServiceClient;
import org.springframework.xml.transform.StringSource;

import javax.xml.transform.Source;

import static org.springframework.ws.test.server.RequestCreators.withPayload;
import static org.springframework.ws.test.server.ResponseMatchers.payload;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes= Application.class)
public class IntegrationTest {

    @Autowired
    private ApplicationContext applicationContext;

    private MockWebServiceClient mockClient;

    @Before
    public void createClient() {
        mockClient = MockWebServiceClient.createClient(applicationContext);
    }

    @Test
    public void customerEndpoint() throws Exception {
        Source requestPayload = new StringSource(
                "<GetPatientDetailsRequest xmlns='http://www.pharmapartners.nl/services/patientservice/v1'> " +
                        "<BSN>12345678</BSN>" +
                        "</GetPatientDetailsRequest>");
        Source responsePayload = new StringSource(
                "<ns2:GetPatientDetailsResponse xmlns:ns2='http://www.pharmapartners.nl/services/patientservice/v1'> " +
                        "<ns2:Patient>" +
                        "<ns2:BSN>12345678</ns2:BSN>" +
                        "<ns2:FirstName>FirstName</ns2:FirstName>" +
                        "<ns2:Name>Name</ns2:Name>" +
                        "<ns2:Initials>Initials</ns2:Initials>" +
                        "<ns2:Birthdate>1986-11-21+01:00</ns2:Birthdate>" +
                        "<ns2:Gender>M</ns2:Gender>" +
                        "</ns2:Patient>" +
                        "<ns2:ResultCode>OK</ns2:ResultCode>" +
                        "</ns2:GetPatientDetailsResponse>");

        mockClient.sendRequest(withPayload(requestPayload)).
                andExpect(payload(responsePayload));
    }
}
