# Soap webservice case

Implementation of soap webservice case using Gradle, Spring ws, Spring boot, Hibernate, JPA and H2

## Testing the application

Project includes unit and integration tests

It is also possible to run the application and use SOAP UI to send a request. 
By default a patient is inserted in the database with bsn: 12345678
Use this bsn to send the request. 
When submitting a different bsn the application will return response with bad request code

## Remarks and possible improvements
    - Implement the birth date mapping to xmlGregorianCalendar through Hibernate bindings
    - Move repository and patient Object to seperate module for reusability
    - I like to write self explanatory code, but it can be debated that move Javadoc may improve the application


## Authors

* **Alain-Michel Leenhouwers**
