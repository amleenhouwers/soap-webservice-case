
package nl.pharmapartners.services.patientservice.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the nl.pharmapartners.services.patientservice.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Patient_QNAME = new QName("http://www.pharmapartners.nl/services/patientservice/v1", "Patient");
    private final static QName _GetPatientDetailsRequest_QNAME = new QName("http://www.pharmapartners.nl/services/patientservice/v1", "GetPatientDetailsRequest");
    private final static QName _GetPatientDetailsResponse_QNAME = new QName("http://www.pharmapartners.nl/services/patientservice/v1", "GetPatientDetailsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: nl.pharmapartners.services.patientservice.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetPatientDetailsRequest }
     * 
     */
    public GetPatientDetailsRequest createGetPatientDetailsRequest() {
        return new GetPatientDetailsRequest();
    }

    /**
     * Create an instance of {@link GetPatientDetailsResponse }
     * 
     */
    public GetPatientDetailsResponse createGetPatientDetailsResponse() {
        return new GetPatientDetailsResponse();
    }

    /**
     * Create an instance of {@link Patient }
     * 
     */
    public Patient createPatient() {
        return new Patient();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Patient }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.pharmapartners.nl/services/patientservice/v1", name = "Patient")
    public JAXBElement<Patient> createPatient(Patient value) {
        return new JAXBElement<Patient>(_Patient_QNAME, Patient.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPatientDetailsRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.pharmapartners.nl/services/patientservice/v1", name = "GetPatientDetailsRequest")
    public JAXBElement<GetPatientDetailsRequest> createGetPatientDetailsRequest(GetPatientDetailsRequest value) {
        return new JAXBElement<GetPatientDetailsRequest>(_GetPatientDetailsRequest_QNAME, GetPatientDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPatientDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.pharmapartners.nl/services/patientservice/v1", name = "GetPatientDetailsResponse")
    public JAXBElement<GetPatientDetailsResponse> createGetPatientDetailsResponse(GetPatientDetailsResponse value) {
        return new JAXBElement<GetPatientDetailsResponse>(_GetPatientDetailsResponse_QNAME, GetPatientDetailsResponse.class, null, value);
    }

}
